﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScript : MonoBehaviour {

	Text KillText;

	// Use this for initialization
	void Start () {
		KillText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (ScoreScript.scoreNum == 1) {
			KillText.text = "You got 1 kill!";
		} else if (ScoreScript.scoreNum == 0) { 
			KillText.text = "You got no kills";
		}else if(ScoreScript.scoreNum > 1){
			KillText.text = "You got " + ScoreScript.scoreNum + " kills!";
		}
	}
}

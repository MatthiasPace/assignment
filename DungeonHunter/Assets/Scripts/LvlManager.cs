﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlManager : MonoBehaviour {
	// Use this for initialization
	void Start () {

	}

	public void MainMenu(){
		LivesScript.livesCount = 3;
		TimerScript.timeLeft = 60f;
		ScoreScript.scoreNum = 0;
		SoundManagerScript.menuSound ();
		Application.LoadLevel (0);
	}

	public void Game() {
		SoundManagerScript.menuSound ();
		Application.LoadLevel (1);
	}

	public void Instructions(){
		SoundManagerScript.menuSound ();
		Application.LoadLevel (2);
	}

	public void EndGame(){
		SoundManagerScript.menuSound ();
		Application.Quit();
	}
}
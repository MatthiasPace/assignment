﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LootBoxAbilityScript : MonoBehaviour {

	public Image cooldownImage;
	public float coolDownTimer = 10f;
	public float timeLeft;
	public static bool reload;
	public Transform[] spawnPoints;
	public GameObject[] loot;
	int randomSpawnPoint, randomLoot;

	// Use this for initialization
	void Start () {
		cooldownImage.gameObject.SetActive (false);
		reload = true;
	}

	// Update is called once per frame
	void Update () {
		if(reload && (Input.GetKeyDown (KeyCode.R))){
				spawnLoot ();
				timeLeft = coolDownTimer;
				StartCoroutine ("Reload");
		}
	}

	public void spawnLoot () {
		randomSpawnPoint = Random.Range (0, spawnPoints.Length);
		randomLoot = Random.Range (0, loot.Length);
		Instantiate (loot [randomLoot], spawnPoints [randomSpawnPoint].position, Quaternion.identity);
	}

	IEnumerator Reload(){
		reload = false;
		cooldownImage.gameObject.SetActive (true);
		while (timeLeft > 0) {
			cooldownImage.fillAmount = timeLeft / coolDownTimer;
			timeLeft -= Time.deltaTime;
			yield return null;
		}
		cooldownImage.gameObject.SetActive (false);
		reload = true;
	}
}

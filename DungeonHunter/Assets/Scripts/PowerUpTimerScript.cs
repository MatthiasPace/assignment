﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpTimerScript : MonoBehaviour {

	static Text text;
	public static float timeLeft = 10f;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void update(){
		while (timeLeft > 0) {
			timeLeft -= Time.deltaTime;
			if (timeLeft < 0) {
				timeLeft = 0;
				text.text = "";
			}
			text.text = "Time left: " + Mathf.Round (timeLeft);
		}
	}
}

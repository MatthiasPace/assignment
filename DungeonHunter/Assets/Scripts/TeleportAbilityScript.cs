﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeleportAbilityScript : MonoBehaviour {

	public Image cooldownImage;
	public float coolDownTimer = 10f;
	public float timeLeft;
	public static bool reload;
	public Transform TeleportTo;
	public GameObject Character;

	// Use this for initialization
	void Start () {
		cooldownImage.gameObject.SetActive (false);
		reload = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(reload && (Input.GetKeyDown (KeyCode.T))){
				Character.transform.position = TeleportTo.transform.position;
				timeLeft = coolDownTimer;
				StartCoroutine ("Reload");
		}
	}

	IEnumerator Reload(){
		reload = false;
		cooldownImage.gameObject.SetActive (true);
		while (timeLeft > 0) {
			cooldownImage.fillAmount = timeLeft / coolDownTimer;
			timeLeft -= Time.deltaTime;
			yield return null;
		}
		cooldownImage.gameObject.SetActive (false);
		reload = true;
	}
}

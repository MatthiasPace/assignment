﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {

	public static AudioClip GunSound, EnemyHit, Powerup, MenuSound;
	static AudioSource audioSrc;

	// Use this for initialization
	void Start () {
		GunSound = Resources.Load<AudioClip> ("GunSound");
		EnemyHit = Resources.Load<AudioClip> ("EnemyHit");
		Powerup = Resources.Load<AudioClip> ("Powerup");
		MenuSound = Resources.Load<AudioClip> ("MenuSound");
		audioSrc = GetComponent <AudioSource> ();
	}

	public static void gunSound (){
		audioSrc.PlayOneShot (GunSound);
	}

	public static void enemyHit (){
		audioSrc.PlayOneShot (EnemyHit);
	}

	public static void powerup (){
		audioSrc.PlayOneShot (Powerup);
	}

	public static void menuSound(){
		audioSrc.PlayOneShot (MenuSound);
	}
}
